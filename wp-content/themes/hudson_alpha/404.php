<?php
/* The template for displaying 404 pages (Not Found). */
get_header(); ?>

<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<section class="content-padder error-404 not-found">
			<header class="page-header">
				<h2 class="page-title">
					Oops! Something went wrong here.
				</h2>
			</header><!-- .page-header -->
			<div class="page-content">
				<p>
					Nothing could be found at this location. Maybe try a search?
				</p>
				<?php get_search_form(); ?>
			</div><!-- .page-content -->
		</section><!-- .content-padder -->
	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>