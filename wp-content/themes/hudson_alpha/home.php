<?php
/* Template Name: Home */
get_header();
?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
