<?php
/* Template Name: Research */
get_header();
?>

<div class="col-sm-8">
	<div id="content" class="site-content">
		<?php while (have_posts()) : the_post(); ?>
		<h1 class="title"><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<ul class="news items">
			<?php
				$args = array(
					'post_type'      => 'researchprojects',
					'post_status'    => 'publish',
					'posts_per_page' => 1000
				);
				$my_query = new WP_Query( $args );
				if ( $my_query->have_posts() ) {
					while ( $my_query->have_posts() ) : $my_query->the_post();
			?>
			<li class="news item">
				<div class="sidebar-title">
					<a href="<?php echo get_the_permalink(); ?>">
						<div class="news-title"><?php echo get_the_title(); ?></div>
					</a>
					<div class="news-date"><?php echo get_the_date( 'F j, Y' ); ?></div>
				</div>
				
				<div class="title-underline"><div class="dot"></div></div>
				<div class="sidebar-excerpt">
					<?php echo get_the_excerpt(); ?>
				</div>
				 
				<?php include TEMPLATEPATH . "/share.php"; ?>

			</li>
			<?php 
			endwhile; 
			}
			wp_reset_query();
			?>

		</ul>
		<div class="paginator"></div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>