<?php

// this script is used for creating pages and menus automatically for hudson alpha


// Add data to menus when activated
function prepopulate_menus() {

	// get_option('menu_check');

  	if ( !(has_nav_menu( 'primary' ) ) ) { 
		create_page('Home');
		create_page('About');
		create_page('Publications');
		create_page('News');
		create_page('Research Projects');
		create_page('Lab Members');
		create_page('Contact');
		create_page('Selected publications in the sidebar');

	}

	// Does the menu exist already?
	$menu_exists = wp_get_nav_menu_object( $menuname );

	// If it doesn't exist, let's create it.
	if( !$menu_exists){

	   	$menu_name = 'Hudson Alpha';
	    //create the menu
	    $menu_id = wp_create_nav_menu($menu_name);
	    //then get the menu object by its name
	    $menu = get_term_by( 'name', $menu_name, 'nav_menu' );

	    // ASSIGN MENU TO LOCATION
	    // $locations = get_theme_mod('nav_menu_locations');
	    // $locations['main-menu'] = $menu->term_id;
	    // set_theme_mod( 'nav_menu_locations', $locations );

	    // $menu_id = wp_create_nav_menu('Main Navigation'); //create the menu
	    // $locations = get_theme_mod('nav_menu_locations'); //get the menu locations
	    // $locations['main-menu'] = 'primary'; //set our new menu to be the main nav
	    // set_theme_mod('nav_menu_locations', $locations); //update 
	    // update_option('menu_check', true);


	    // Set up default links and add them to the menu.
	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Home'),
	        'menu-item-classes' => 'home',
	        'menu-item-url' => home_url( '/' ), 
	        'menu-item-position' => 0,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('About'),
	        'menu-item-classes' => 'About',
	        'menu-item-url' => home_url( '/about/' ), 
	        'menu-item-position' => 1,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Publications'),
	        'menu-item-classes' => 'publications',
	        'menu-item-url' => home_url( '/publications/' ), 
	        'menu-item-position' => 2,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('News'),
	        'menu-item-classes' => 'news',
	        'menu-item-url' => home_url( '/news/' ), 
	        'menu-item-position' => 4,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Research Projects'),
	        'menu-item-classes' => 'research-projects',
	        'menu-item-url' => home_url( '/research-projects/' ), 
	        'menu-item-position' => 5,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Lab Members'),
	        'menu-item-classes' => 'lab-members',
	        'menu-item-url' => home_url( '/lab-members/' ), 
	        'menu-item-position' => 6,
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Research Postions'),
	        'menu-item-classes' => 'research-postions',
	        'menu-item-position' => 7,
	        'menu-item-url' => 'http://hudsonalpha.applicantpro.com/jobs/',
	        'menu-item-target' => '_blank',
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Contact'),
	        'menu-item-classes' => 'contact',
	        'menu-item-url' => home_url( '/contact/' ), 
	        'menu-item-position' => 8,
	        'menu-item-status' => 'publish'));

		// ASSIGN MENU TO LOCATION
		// 	    $theme = get_current_theme();
		// $mods = get_option("mods_$theme");
		// $key = key($mods['nav_menu_locations']);
		// $mods['nav_menu_locations'][$key] = $menu_id;
		// update_option("mods_$theme", $mods);

	}
} 
add_action( 'init', 'prepopulate_menus' );


// Force permalinks to postname
function reset_permalinks() {
		    // Use a static front page
	$home = get_page_by_title( 'Home' );
	update_option( 'page_on_front', $home->ID );
	update_option( 'show_on_front', 'page' );

	// force permalinks to postname
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure( '/%postname%/' );
}
add_action( 'init', 'reset_permalinks' );


function create_page($new_page_title) {

	if (isset($_GET['activated']) && is_admin()){
        $new_page_content = '';
        $new_page_template = ''; //ex. template-custom.php. Leave blank if you don't want a custom page template.

        $page_check = get_page_by_title($new_page_title);
        $new_page = array(
                'post_type' => 'page',
                'post_title' => $new_page_title,
                'post_content' => $new_page_content,
                'post_status' => 'publish',
                'post_author' => 1,
        );
        if ( !isset($page_check->ID )){
            $new_page_id = wp_insert_post($new_page);
            if(!empty($new_page_template)){
                    update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
            }
        }
	}
}
