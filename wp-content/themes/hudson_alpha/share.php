<!-- https://simplesharebuttons.com/html-share-buttons/ -->
<div id="share-buttons">
 
	<!-- Facebook -->
	<a href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>&t=<?php echo get_the_title(); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/share/facebook.png" alt="Facebook" /></a>
	 
	<!-- Twitter -->
	<a href="http://twitter.com/share?url=<?php echo get_the_permalink(); ?>&text=<?php echo get_the_title(); ?>&hashtags=HudsonAlpha" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/share/twitter.png" alt="Twitter" /></a>
	 
	<!-- Google+ -->
	<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/share/google.png" alt="Google" /></a>
	 
	<!-- LinkedIn -->
	<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink(); ?>" target="_blank"><img src="<?php echo get_bloginfo('template_url'); ?>/images/share/linkedin.png" alt="LinkedIn" /></a>
	 
	<!-- Email -->
	<a href="mailto:?Subject=Hudson Alpha News Link&Body=I%20thought%20you%20might%20find%20this%20interesting.%20 <?php echo get_the_permalink(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/share/email.png" alt="Email" /></a>
</div>



<style type="text/css">
 
	#share-buttons img {
		width: 35px;
		padding: 5px;
		border: 0;
		box-shadow: 0;
		display: inline;
	}
 
</style>
