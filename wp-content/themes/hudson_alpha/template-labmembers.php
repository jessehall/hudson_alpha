<?php
/* Template Name: Lab members */
get_header();
?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="top"></div>
	<div id="content" class="site-content">
		<?php while ( have_posts() ) : the_post(); ?>
		<h1 class="title"><?php the_title() ?></h1>
		<?php endwhile; ?>

		<?php
		$args     = array(
			'post_type'      => 'lab_members',
			'post_status'    => 'publish',
			'posts_per_page' => - 1
		);
		$my_query = new WP_Query( $args );
		if ( $my_query->have_posts() ) {
		?>
		<?php
		$i = 0;
		while ( $my_query->have_posts() ) :
			$my_query->the_post();
			if ( $i == 0 ) {
				echo '<div class="row">';
			}
			?>
			<div class="col-lg-6 col-md-6 col-sm-6 person">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<?php
						if(has_post_thumbnail()) {
							echo '<img src="'.wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())).'" alt="" />';
						} else {
							echo '<img src="'.get_template_directory_uri() . '/images/default_member.jpg" alt="" />';
						}
						?>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="lab-members-title">
							<?php echo get_the_title(); ?>
						</div>
						<div class="content">
							<span class="subtitle">
								<?php
								$cv_position = get_post_meta( get_the_ID(), 'cv_position', true );
								// check if the custom field has a value
								if( ! empty( $cv_position ) ) {
									echo $cv_position;
								}
								?>
							</span>
							<span class="subtitle">
								<?php
								$cv_email = get_post_meta( get_the_ID(), 'cv_email', true );
								// check if the custom field has a value
								if( ! empty( $cv_email ) ) { ?>
								<a href="mailto:<?php echo $cv_email; ?>" ><?php echo $cv_email; ?></a>
								<?php } ?>
							</span>
							<span class="subtitle">
							<?php
								$cv_telephone = get_post_meta( get_the_ID(), 'cv_telephone', true );
								// check if the custom field has a value
								if( ! empty( $cv_telephone ) ) {
									echo $cv_telephone;
								}
								?>
							</span>
						</div>
					</div>
				</div>
			</div>
			<?php
		if ( $i % 2 != 0 ) {
			echo '</div><div class="row">';
		}
		if ( ( $i + 1 ) === $my_query->post_count ) {
	echo '</div>';
	}
	$i ++;
	endwhile; 
	}
	wp_reset_query();
	?>
	</div>
	<div class="past-members">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>