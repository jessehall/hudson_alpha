<?php
/* Template Name: Publications */
get_header();
?>
<div class="col-lg-8 col-md-8 col-sm-8">
	<div id="content" class="site-content">
		<? while ( have_posts() ) : the_post(); ?>
		<h1 class="title"><? the_title() ?></h1>
		<? the_content(); ?>
		<?php endwhile; ?>
		<div class="paginator"></div>
		<ul class="publications">
			<?php
				$term_array = get_post_custom_values( "pubmed" );
				$term = $term_array[0];
				if ( $term != '' ) {
					include_once( TEMPLATEPATH . '/PubMedAPI.php' );
					$PubMedAPI = new PubMedAPI();
					$results = $PubMedAPI->query( $term, false );
				}
			?>

			<?php if ( ! empty( $results ) ) { ?>
				<?php $pub_count = 1; ?>
				<?php foreach ( $results as $result ) { ?>

					<li class="publication"><h5><?php echo $pub_count.'.)'; ?> <a href="http://www.ncbi.nlm.nih.gov/pubmed/<?php echo $result['pmid']; ?>"
							target="_blank"><?php echo $result['title']; ?></h5></a>
						<?php $authors_results = implode( ", ", array_splice( $result['authors'], 0, 20 )); ?>
						Authors: <?php echo $authors_results . " ... <br>"; ?>
						Abstract: <?php if ($result['abstract']) {echo $result['abstract']; } else { echo "n/a";}?>
						<?php //echo $result['journalabbrev']<br />; ?>
						Year: <?php echo $result['year']; ?>
						PMID: <?php echo $result['pmid']; ?>
					</li>

					<?php 
						// if ($pub_count%7 == 1)
						//     {  
						//          echo "<!--more-->";
						//     }
					?>

					<? $pub_count++; ?>
				<?php } ?>
			<?php } ?>
		</ul>
		<div class="paginator"></div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>