<?php
// Add read more to news content in sidebar
function new_excerpt_more( $more ) {
	global $post;
	return $post->post_content . ' <a href="' . get_permalink( $post->ID ) . '">' . '<br />[ Read More ]' . '</a>';
}
add_filter( 'get_the_excerpt', 'new_excerpt_more' );
?>

<style type="text/css">

	.paginator ul li.active {
		border: 1px solid #ccc;
		padding 5px;
	}

	.paginator ul li.active span.current {
		padding: 10px;
	}

	.paginator ul li:first-child, .paginator ul li:last-child {
    	border: none;
	}	

</style>

<div class="col-lg-4 col-md-4 col-sm-4">
	<div class="sidebar">

		<?php if (get_page_by_title('news')) {  // if the news page exists display here in side bar ?>
		<div class="b-side-block sidebar-news">
			<div class="title">News</div>
			<div class="content">
				<ul class="items">
					<?php
						function the_excerpt_max_charlength($charlength) {
							$excerpt = get_the_excerpt();
							$charlength++;

							if ( mb_strlen( $excerpt ) > $charlength ) {
								$subex = mb_substr( $excerpt, 0, $charlength - 5 );
								$exwords = explode( ' ', $subex );
								$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
								if ( $excut < 0 ) {
									echo mb_substr( $subex, 0, $excut );
								} else {
									echo $subex;
								}
								echo '[...]';
							} else {
								echo $excerpt;
							}
						}

						$args = array(
							'post_type'      => 'news',
							'post_status'    => 'publish',
							'posts_per_page' => 3
						);

						$my_query = new WP_Query( $args );

						if ( $my_query->have_posts() ) {
							while ( $my_query->have_posts() ) : $my_query->the_post();
					?>

					<li class="item">
						<div class="sidebar-title">
							<span class="sidebar-date"><?php print get_the_date( 'F j, Y' ); ?></span> | 
							<a href="<?php print get_the_permalink(); ?>" target="_blank"><?php print get_the_title(); ?></a>
						</div>
						<div class="sidebar-excerpt">
							<?php 
								the_excerpt_max_charlength(140);
							?>
						</div>
					</li>
					<?php 	endwhile; 
						} else {
							echo '
								<li class="item">
									No News to display at this time.
								</li>';
						}
						wp_reset_query();
					?>
				</ul>
				<div class="more">
					<a href="<?php print bloginfo( 'url' ) . '/news/'; ?>">All news</a>
				</div>
			</div>
		</div>
	<?php } // END DISPALY OF NEWS IN SIDEBAR ?>

		<div class="b-side-block sidebar-publications">
			<div class="title">
				Latest Publications
			</div>
			<div class="content">
				<ul class="items">
					<li class="item">
						<?php
							$page_slug = 'selected-publications-in-sidebar';

							if (get_page_by_path($page_slug)) {
								$get_page_slug = get_page_by_path( $page_slug );
							    $page_id = $get_page_slug->ID;
							    $page_data = get_page( $page_id );  
							    echo apply_filters('the_content', $page_data->post_content);

							} else {
								echo "No Publications to display at this time.";
							}
						?>
					</li>
				</ul>
				<div class="more">
					<a href="<?php print bloginfo( 'url' ) . '/publications/'; ?>">All publications</a>
				</div>
			</div>
		</div> 
	</div>
</div>